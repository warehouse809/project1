variable "server_port" {
    description = "The port the server will use for HTTP request"
    type = number
    default = 8080
}
variable "cluster_name" {
    description = "The name to use for all the cluster resources"
    type = string
}
#output "public_ip" {
    #value = aws_launch_configuration.example.public_ip
    #description = "Public IP of web server"
#}

output "alb_dns_name" {
    value = aws_lb.example.dns_name
    description = "The domain name of the load balancer"
}
provider "aws" {
    region="us-east-2"
}

module "webserver_cluster" {
    source = "C:/Users/sctang/git/project1/Modules/webserver-cluster"  

    cluster_name = "webserver-stage"
}
terraform {
  backend "s3"{
  bucket = "terraform3bucket"
  key = "stage/webserver-cluster/terraform.tfstate"
  region = "us-east-2"

  dynamodb_table = "terraform-up-and-running-locks2"
  encrypt = true
  }
}

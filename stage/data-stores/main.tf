provider "aws" {
    region="us-east-2"
}

resource "aws_db_instance" "example" {
    identifier_prefix = "terraform-up-and-running"
    engine = "mysql"
    allocated_storage = 10
    instance_class = "db.t2.micro"
    name = "example_database"
    username = "admin"

    password = "Test9999"
}

terraform {
  backend "s3"{
      bucket = "terraform3bucket"
      key ="stage/data-stores/terraform.tfstate"
      region = "us-east-2"

      dynamodb_table = "terraform-up-and-running-locks"
      encrypt = true
  }
}